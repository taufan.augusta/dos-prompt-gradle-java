package dos;

public class Prompt
{
    public String fullPrompt;
    public int size;
    public String command;
    public String source;
    public String destination;

    public Prompt(){
        this.fullPrompt = "Unknown Prompt Pattern";
        this.size = 0;
        this.command = null;
        this.source = null;
        this.destination = null;
    }

    public Prompt(String command){
        this.fullPrompt = command;
        this.size = 1;
        this.command = command;
        this.source = null;
        this.destination = null;
    }

    public Prompt(String command, String source){
        this.fullPrompt = command + " " + source;
        this.size = 2;
        this.command = command;
        this.source = source;
        this.destination = null;
    }

    public Prompt(String command, String source, String destination){
        this.fullPrompt = command + " " + source + " " + destination;
        this.size = 3;
        this.command = command;
        this.source = source;
        this.destination = destination;
    }
}